FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > zsh.log'

COPY zsh .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' zsh
RUN bash ./docker.sh

RUN rm --force --recursive zsh
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD zsh
